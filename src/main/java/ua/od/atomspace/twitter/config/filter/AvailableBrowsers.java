package ua.od.atomspace.twitter.config.filter;

public enum AvailableBrowsers {
    CHROME,
    OPERA,
    FIREFOX,
    SAFARI
}

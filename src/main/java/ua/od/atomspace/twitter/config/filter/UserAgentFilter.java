package ua.od.atomspace.twitter.config.filter;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Stream;

@Component
@Slf4j
public class UserAgentFilter implements Filter {

    private static String IMG_URL = "https://i.imgur.com/MscMnMh.png";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;

        String userAgent = httpRequest.getHeader("User-Agent");
        boolean browserIsAvailable = Stream.of(AvailableBrowsers.values())
                .anyMatch(availableBrowser -> StringUtils.containsIgnoreCase(userAgent, availableBrowser.toString()));
        if (!browserIsAvailable) {
            log.warn("{} tried to get access from unavailable browser", httpRequest.getRemoteAddr());
            HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
            httpResponse.sendRedirect(IMG_URL);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {
    }

}

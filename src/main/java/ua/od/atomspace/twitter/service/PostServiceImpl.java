package ua.od.atomspace.twitter.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.od.atomspace.twitter.dao.model.Post;
import ua.od.atomspace.twitter.dao.model.User;
import ua.od.atomspace.twitter.dao.repository.PostRepository;
import ua.od.atomspace.twitter.dao.repository.UserRepository;
import ua.od.atomspace.twitter.dto.PostDto;
import ua.od.atomspace.twitter.exception.ResourceNotFoundException;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static ua.od.atomspace.twitter.service.UserServiceImpl.logUserNotFound;

@Service
@Slf4j
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, UserRepository userRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }


    @Override
    public List<PostDto> list(Long userId) {
        return findUserById(userId).getPosts()
                .parallelStream()
                .map(this::buildToDto)
                .collect(Collectors.toList());
    }


    @Override
    public PostDto get(Long postId) {
        Post postById = postRepository.findById(postId)
                .orElseThrow(() -> logPostNotFound(postId));
        return buildToDto(postById);
    }

    @Override
    public PostDto create(PostDto request) {
        Post post = postRepository.save(buildToEntity(request));
        return buildToDto(post);
    }

    @Override
    public PostDto update(PostDto request) {
        Post post = postRepository.findById(request.getPostId())
                .orElseThrow(() -> logPostNotFound(request.getPostId()));
        post.setMessage(request.getMessage());
        post.setTitle(request.getTitle());
        Post response = postRepository.save(post);
        return buildToDto(response);
    }

    @Override
    public PostDto delete(Long postId) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> logPostNotFound(postId));
        postRepository.delete(post);
        return buildToDto(post);
    }

    @Override
    public List<PostDto> getBySubtitle(Long userId, String subtitle) {
        List<Post> list = postRepository.findAllByTitleIgnoreCaseContainingAndUserUserId(subtitle, userId);
        List<PostDto> response = new LinkedList<>();
        list.forEach(post -> response.add(buildToDto(post)));
        return response;
    }

    private User findUserById(Long userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> logUserNotFound(userId));
    }

    public static ResourceNotFoundException logPostNotFound(Long id) {
        log.error("Post with id = {} NOT_FOUND", id);
        return new ResourceNotFoundException("Post with id = " + id + " NOT_FOUND");
    }

    private PostDto buildToDto(Post post) {
        return PostDto.builder()
                .postId(post.getPostId())
                .userId(post.getUser().getUserId())
                .message(post.getMessage())
                .title(post.getTitle())
                .createdAt(post.getCreatedAt())
                .updatedAt(post.getUpdatedAt())
                .build();
    }

    private Post buildToEntity(PostDto postDto) {
        Post post = new Post();
        User user = findUserById(postDto.getUserId());
        post.setTitle(postDto.getTitle());
        post.setMessage(postDto.getMessage());
        post.setUser(user);
        return post;
    }

}

package ua.od.atomspace.twitter.service;

import ua.od.atomspace.twitter.dto.PostDto;

import java.util.List;

public interface PostService {

    List<PostDto> list(Long userId);

    PostDto get(Long postId);

    PostDto create(PostDto postDto);

    PostDto update(PostDto postDto);

    PostDto delete(Long postId);

    List<PostDto> getBySubtitle(Long userId, String subtitle);


}

package ua.od.atomspace.twitter.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ua.od.atomspace.twitter.dao.model.Like;
import ua.od.atomspace.twitter.dao.model.Post;
import ua.od.atomspace.twitter.dao.model.ReferenceUserPostKey;
import ua.od.atomspace.twitter.dao.model.User;
import ua.od.atomspace.twitter.dao.repository.LikeRepository;
import ua.od.atomspace.twitter.dao.repository.PostRepository;
import ua.od.atomspace.twitter.dao.repository.UserRepository;
import ua.od.atomspace.twitter.dto.LikeDto;
import ua.od.atomspace.twitter.exception.ResourceNotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ua.od.atomspace.twitter.service.PostServiceImpl.logPostNotFound;
import static ua.od.atomspace.twitter.service.UserServiceImpl.logUserNotFound;

@Service
@Slf4j
public class LikeServiceImpl implements LikeService {

    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final LikeRepository likeRepository;

    @Autowired
    public LikeServiceImpl(UserRepository userRepository, PostRepository postRepository, LikeRepository likeRepository) {
        this.userRepository = userRepository;
        this.postRepository = postRepository;
        this.likeRepository = likeRepository;
    }

    @Override
    public List<LikeDto> list(Long postId) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> logPostNotFound(postId));
        return post.getLikes()
                .parallelStream()
                .map(this::buildToDto)
                .collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<LikeDto> likeDislike(Long postId, Long userId) {
        ReferenceUserPostKey referenceUserPostKey = new ReferenceUserPostKey(userId, postId);

        Optional<Like> likeInBase = likeRepository.findById(referenceUserPostKey);
        if (likeInBase.isPresent()) {
            likeRepository.delete(likeInBase.get());
            LikeDto likeDto = buildToDto(likeInBase.get());
            log.info("DELETE like {}", likeDto);
            return new ResponseEntity<>(likeDto, HttpStatus.OK);
        }
        Like likeToSave = new Like();
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> logPostNotFound(postId));
        User user = userRepository.findById(userId)
                .orElseThrow(() -> logUserNotFound(userId));

        likeToSave.setReferenceUserPostKey(referenceUserPostKey);
        likeToSave.setPost(post);
        likeToSave.setUser(user);

        LikeDto likeDto = buildToDto(likeRepository.save(likeToSave));
        log.info("CREATE like {}", likeDto);
        return new ResponseEntity<>(likeDto, HttpStatus.CREATED);
    }

    LikeDto buildToDto(Like like) {
        return LikeDto.builder()
                .createdAt(like.getCreatedAt())
                .postId(like.getPost().getPostId())
                .userId(like.getUser().getUserId())
                .build();
    }

    Like buildToEntity(LikeDto dto) {
        Like like = new Like();

        Post post = postRepository.findById(dto.getPostId())
                .orElseThrow(() -> {
                    log.error("Post with id = {} was not found", dto.getPostId());
                    return new ResourceNotFoundException();
                });
        User user = userRepository.findById(dto.getUserId())
                .orElseThrow(() -> {
                    log.error("User with id = {} was not found", dto.getUserId());
                    return new ResourceNotFoundException();
                });

        like.setCreatedAt(dto.getCreatedAt());
        like.setPost(post);
        like.setUser(user);

        return like;
    }
}

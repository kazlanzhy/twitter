package ua.od.atomspace.twitter.service;

import org.springframework.http.ResponseEntity;
import ua.od.atomspace.twitter.dto.UserDto;

import java.util.List;

public interface UserService {

    List<UserDto> list();

    UserDto get(Long id);

    UserDto create(UserDto userDto);

    UserDto update(UserDto userDto, Long id);

    UserDto delete(Long id);

    List<UserDto> getUsersByLastName(String lastName);

    List<UserDto> getUsersByAgeBetween(Short startAge, Short endAge);

    List<UserDto> getUsersByCreationTime();

    Integer getUsersCountWithFirstNameStartingWith(String str);

    List<UserDto> getFollowing(Long userId);

    List<UserDto> getFollowers(Long userId);

    ResponseEntity<UserDto> tryFollow(Long userId, Long followingUserId);


}

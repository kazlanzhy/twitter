package ua.od.atomspace.twitter.service;

import org.springframework.http.ResponseEntity;
import ua.od.atomspace.twitter.dto.LikeDto;

import java.util.List;

public interface LikeService {

    List<LikeDto> list(Long postId);

    ResponseEntity<LikeDto> likeDislike(Long postId, Long userId);

}

package ua.od.atomspace.twitter.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.od.atomspace.twitter.dao.model.Comment;
import ua.od.atomspace.twitter.dao.model.Post;
import ua.od.atomspace.twitter.dao.model.User;
import ua.od.atomspace.twitter.dao.repository.CommentRepository;
import ua.od.atomspace.twitter.dao.repository.PostRepository;
import ua.od.atomspace.twitter.dao.repository.UserRepository;
import ua.od.atomspace.twitter.dto.CommentDto;
import ua.od.atomspace.twitter.exception.ResourceNotFoundException;

import java.util.List;
import java.util.stream.Collectors;

import static ua.od.atomspace.twitter.service.PostServiceImpl.logPostNotFound;
import static ua.od.atomspace.twitter.service.UserServiceImpl.logUserNotFound;

@Service
@Slf4j
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository, UserRepository userRepository, PostRepository postRepository) {
        this.commentRepository = commentRepository;
        this.userRepository = userRepository;
        this.postRepository = postRepository;
    }

    public List<CommentDto> list(Long postId) {
        return findPostById(postId).getComments()
                .parallelStream()
                .map(this::buildToDto)
                .collect(Collectors.toList());
    }

    public CommentDto create(CommentDto dto) {
        Comment comment = commentRepository.save(buildToEntity(dto));
        return buildToDto(comment);
    }

    public CommentDto update(CommentDto dto) {
        Comment comment = commentRepository.findById(dto.getCommentId())
                .orElseThrow(() -> logCommentNotFound(dto.getCommentId()));
        comment.setMessage(dto.getMessage());

        Comment response = commentRepository.save(comment);
        return buildToDto(response);
    }

    public CommentDto delete(Long commentId) {
        Comment comment = commentRepository.findById(commentId)
                .orElseThrow(() -> logCommentNotFound(commentId));
        commentRepository.delete(comment);
        return buildToDto(comment);
    }

    private User findUserById(Long userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> logUserNotFound(userId));
    }

    private Post findPostById(Long postId) {
        return postRepository.findById(postId)
                .orElseThrow(() -> logPostNotFound(postId));
    }

    public static ResourceNotFoundException logCommentNotFound(Long id) {
        log.error("Comment with id = {} NOT_FOUND", id);
        return new ResourceNotFoundException();
    }

    private CommentDto buildToDto(Comment comment) {
        return CommentDto.builder()
                .commentId(comment.getCommentId())
                .createdAt(comment.getCreatedAt())
                .message(comment.getMessage())
                .postId(comment.getPost().getPostId())
                .userId(comment.getUser().getUserId())
                .build();
    }

    private Comment buildToEntity(CommentDto dto) {
        User user = findUserById(dto.getUserId());
        Post post = findPostById(dto.getPostId());

        Comment comment = new Comment();
        comment.setUser(user);
        comment.setPost(post);
        comment.setMessage(dto.getMessage());

        return comment;
    }
}

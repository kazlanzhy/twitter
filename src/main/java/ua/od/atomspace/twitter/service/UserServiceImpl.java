package ua.od.atomspace.twitter.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ua.od.atomspace.twitter.dao.model.User;
import ua.od.atomspace.twitter.dao.repository.UserRepository;
import ua.od.atomspace.twitter.dto.UserDto;
import ua.od.atomspace.twitter.exception.BadRequestException;
import ua.od.atomspace.twitter.exception.ResourceNotFoundException;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<UserDto> list() {
        Iterable<User> users = userRepository.findAll();
        List<UserDto> list = new LinkedList<>();
        users.forEach(user -> list.add(buildToDto(user)));
        return list;
    }

    @Override
    public UserDto get(Long id) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> logUserNotFound(id));
        return buildToDto(user);
    }

    @Override
    public UserDto create(UserDto request) {
        User user = userRepository.save(buildToEntity(request));
        return buildToDto(user);
    }

    @Override
    public UserDto update(UserDto request, Long id) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> logUserNotFound(id));
        user.setSecondName(request.getSecondName());
        user.setFirstName(request.getFirstName());
        user.setEmail(request.getEmail());
        user.setAge(request.getAge());
        user.setUsername(request.getUsername());
        User response = userRepository.save(user);
        return buildToDto(response);
    }

    @Override
    public UserDto delete(Long id) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> logUserNotFound(id));
        userRepository.delete(user);
        return buildToDto(user);
    }

    @Override
    public List<UserDto> getUsersByLastName(String lastName) {
        List<User> users = userRepository.findAllBySecondNameEquals(lastName);
        List<UserDto> list = new LinkedList<>();
        users.forEach(user -> list.add(buildToDto(user)));
        return list;
    }

    @Override
    public List<UserDto> getUsersByAgeBetween(Short startAge, Short endAge) {
        List<User> users = userRepository.findAllByAgeBetween(startAge, endAge);
        List<UserDto> list = new LinkedList<>();
        users.forEach(user -> list.add(buildToDto(user)));
        return list;
    }

    @Override
    public List<UserDto> getUsersByCreationTime() {
        List<User> users = userRepository.findTop10ByOrderByCreatedAtDesc();
        List<UserDto> list = new LinkedList<>();
        users.forEach(user -> list.add(buildToDto(user)));
        return list;
    }

    @Override
    public Integer getUsersCountWithFirstNameStartingWith(String str) {
        return userRepository.countAllByFirstNameStartingWith(str);
    }

    @Override
    public ResponseEntity<UserDto> tryFollow(Long userId, Long followingUserId) {
        if (userId.equals(followingUserId)) {
            throw new BadRequestException("You cannot follow to yourself");
        }

        User userWhichFollows = userRepository.findById(userId)
                .orElseThrow(() -> logUserNotFound(userId));

        User followingUser = userRepository.findById(followingUserId)
                .orElseThrow(() -> logUserNotFound(followingUserId));

        boolean followed = userRepository.existsUserByFollowing(followingUser);

        if (followed) {
            userWhichFollows.getFollowing().remove(followingUser);
            UserDto userDto = buildToDto(userRepository.save(userWhichFollows));
            log.info("Unfollowed user {} from user {}", userDto, buildToDto(followingUser));
            return new ResponseEntity<>(userDto, HttpStatus.OK);
        }
        userWhichFollows.getFollowing().add(followingUser);
        UserDto userDto = buildToDto(userRepository.save(userWhichFollows));
        log.info("Followed user {} to user {}", userDto, buildToDto(followingUser));
        return new ResponseEntity<>(userDto, HttpStatus.CREATED);
    }

    @Override
    public List<UserDto> getFollowers(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> logUserNotFound(userId));
        return user.getFollowers()
                .stream()
                .map(this::buildToDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<UserDto> getFollowing(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> logUserNotFound(userId));
        return user.getFollowing()
                .stream()
                .map(this::buildToDto)
                .collect(Collectors.toList());
    }

    public static ResourceNotFoundException logUserNotFound(Long userId) {
        log.error("User with id = {} NOT_FOUND", userId);
        return new ResourceNotFoundException("User with id = " + userId + " NOT_FOUND");
    }

    private UserDto buildToDto(User user) {
        return UserDto.builder()
                .user_id(user.getUserId())
                .age(user.getAge())
                .firstName(user.getFirstName())
                .secondName(user.getSecondName())
                .username(user.getUsername())
                .password(user.getPassword())
                .email(user.getEmail())
                .createdAt(user.getCreatedAt())
                .updatedAt(user.getUpdatedAt())
                .build();
    }

    private User buildToEntity(UserDto dto) {
        User user = new User();
        user.setAge(dto.getAge());
        user.setEmail(dto.getEmail());
        user.setFirstName(dto.getFirstName());
        user.setSecondName(dto.getSecondName());
        user.setPassword(dto.getPassword());
        user.setUsername(dto.getUsername());
        return user;
    }

}

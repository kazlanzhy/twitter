package ua.od.atomspace.twitter.service;

import ua.od.atomspace.twitter.dto.CommentDto;

import java.util.List;

public interface CommentService {

    List<CommentDto> list(Long postId);

    CommentDto create(CommentDto dto);

    CommentDto update(CommentDto dto);

    CommentDto delete(Long commentId);
}

package ua.od.atomspace.twitter.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ua.od.atomspace.twitter.dto.UserDto;
import ua.od.atomspace.twitter.service.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    @ResponseBody
    public UserDto create(@Valid @RequestBody UserDto request) {
        UserDto response = userService.create(request);
        log.info("CREATE user {}", response);
        return response;
    }

    @GetMapping()
    @ResponseStatus(code = HttpStatus.OK)
    @ResponseBody
    public List<UserDto> list() {
        List<UserDto> response = userService.list();
        log.info("GET users: {}", response);
        return response;
    }


    @GetMapping("/{id}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public UserDto get(@PathVariable Long id) {
        UserDto response = userService.get(id);
        log.info("GET user by id = {}: {}", id, response);
        return response;
    }

    @GetMapping(params = {"lastName"})
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public List<UserDto> getUsersByLastName(@RequestParam(name = "lastName") String lastName) {
        List<UserDto> response = userService.getUsersByLastName(lastName);
        log.info("GET users by lastName = {}: {}", lastName, response);
        return response;
    }

    @PutMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    @ResponseBody
    public UserDto update(@Valid @RequestBody UserDto request, @PathVariable Long id) {
        UserDto response = userService.update(request, id);
        log.info("UPDATE user by id = {} : {}", id, response);
        return response;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    @ResponseBody
    public UserDto delete(@PathVariable Long id) {
        UserDto response = userService.delete(id);
        log.info("DELETE user by id = {}: {}", id, response);
        return response;
    }

    @GetMapping(params = {"startAge", "endAge"})
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public List<UserDto> getUsersByAgeBetween(@RequestParam(defaultValue = "18") Short startAge,
                                              @RequestParam(defaultValue = "45") Short endAge) {
        List<UserDto> response = userService.getUsersByAgeBetween(startAge, endAge);
        log.info("GET users by age between {} and {}: {}", startAge, endAge, response);
        return response;
    }

    @GetMapping("/creation")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public List<UserDto> getUsersByCreationTime() {
        List<UserDto> response = userService.getUsersByCreationTime();
        log.info("GET 10 users by creation time: {}", response);
        return response;
    }

    @GetMapping(path = "/count", params = {"firstNameStartsWith"})
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Integer getUsersCountWithFirstNameStartingWith(@RequestParam String firstNameStartsWith) {
        Integer response = userService.getUsersCountWithFirstNameStartingWith(firstNameStartsWith);
        log.info("GET user counter = {} with name starting with = '{}'", response, firstNameStartsWith);
        return response;
    }

    @PostMapping("/{userId}/follow/{followingUserId}")
    @ResponseBody
    public ResponseEntity<UserDto> tryFollow(@PathVariable Long userId, @PathVariable Long followingUserId) {
        return userService.tryFollow(userId, followingUserId);
    }

    @GetMapping("/{userId}/followers")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public List<UserDto> getUsersFollowers(@PathVariable Long userId) {
        List<UserDto> response = userService.getFollowers(userId);
        log.info("GET followers for user with id = {}: {}", userId, response);
        return response;
    }

    @GetMapping("/{userId}/following")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public List<UserDto> getUsersFollowing(@PathVariable Long userId) {
        List<UserDto> response = userService.getFollowing(userId);
        log.info("GET following users for user with id = {}: {}", userId, response);
        return response;
    }


}

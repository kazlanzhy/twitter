package ua.od.atomspace.twitter.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ua.od.atomspace.twitter.dto.PostDto;
import ua.od.atomspace.twitter.service.PostService;

import java.util.List;

@RestController
@Slf4j
@RequestMapping
public class PostController {
    private final PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping("/api/users/{userId}/posts")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public List<PostDto> list(@PathVariable Long userId) {
        List<PostDto> response = postService.list(userId);
        log.info("GET posts in user with id = {} : {} ", userId, response);
        return response;
    }

    @GetMapping("/api/posts/{postId}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public PostDto get(@PathVariable Long postId) {
        PostDto response = postService.get(postId);
        log.info("GET post by id = {}: {}", postId, response);
        return response;
    }

    @GetMapping(value = "/api/users/{userId}/posts", params = "subtitle")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<PostDto> get(@PathVariable Long userId, @RequestParam String subtitle) {
        List<PostDto> response = postService.getBySubtitle(userId, subtitle);
        log.info("GET post by subtitle = \"{}\" in user with id = {}: {}", subtitle, userId, response);
        return response;
    }

    @PostMapping("/api/posts")
    @ResponseStatus(code = HttpStatus.CREATED)
    @ResponseBody
    public PostDto create(@RequestBody PostDto request) {
        PostDto response = postService.create(request);
        log.info("POST post in user with id = {}: {}", request.getUserId(), response);
        return response;
    }

    @PutMapping("/api/posts")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public PostDto update(@RequestBody PostDto request) {
        PostDto response = postService.update(request);
        log.info("UPDATE post by id = {} in user with id = {}: {}", request.getPostId(), request.getUserId(), response);
        return response;
    }

    @DeleteMapping("/api/posts/{postId}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public PostDto delete(@PathVariable Long postId) {
        PostDto response = postService.delete(postId);
        log.info("DELETE post by id = {}: {}", postId, response);
        return response;
    }

}

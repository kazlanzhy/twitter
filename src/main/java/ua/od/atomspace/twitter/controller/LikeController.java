package ua.od.atomspace.twitter.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ua.od.atomspace.twitter.dto.LikeDto;
import ua.od.atomspace.twitter.service.LikeService;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/posts/{postId}/likes")
public class LikeController {

    private final LikeService likeService;

    @Autowired
    public LikeController(LikeService likeService) {
        this.likeService = likeService;
    }

    @GetMapping
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public List<LikeDto> list(@PathVariable Long postId) {
        List<LikeDto> response = likeService.list(postId);
        log.info("GET likes for post with id = {}: {} ", postId, response);
        return response;
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<LikeDto> likeDislike(@PathVariable Long postId, @RequestBody LikeDto request) {
        return likeService.likeDislike(postId, request.getUserId());
    }

}

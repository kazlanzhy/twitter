package ua.od.atomspace.twitter.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ua.od.atomspace.twitter.dto.CommentDto;
import ua.od.atomspace.twitter.service.CommentService;

import java.util.List;

@RestController
@Slf4j
@RequestMapping
public class CommentController {

    private CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping("/api/posts/{postId}/comments")
    @ResponseBody
    public List<CommentDto> list(@PathVariable Long postId) {
        List<CommentDto> response = commentService.list(postId);
        log.info("GET comments in post with id = {}: {}", postId, response);
        return response;
    }

    @PostMapping("/api/comments")
    @ResponseStatus(code = HttpStatus.CREATED)
    @ResponseBody
    public CommentDto create(@RequestBody CommentDto request) {
        CommentDto response = commentService.create(request);
        log.info("POST comment in post with id = {} by user with id = {}: {}", response.getPostId(), response.getUserId(), response);
        return response;
    }

    @PutMapping("/api/comments")
    @ResponseBody
    public CommentDto update(@RequestBody CommentDto request) {
        CommentDto response = commentService.update(request);
        log.info("UPDATE comment in post with id = {} : {}", response.getPostId(), response);
        return response;
    }

    @DeleteMapping("/api/comments/{commentId}")
    @ResponseBody
    public CommentDto delete(@PathVariable Long commentId) {
        CommentDto response = commentService.delete(commentId);
        log.info("DELETE comment with id = {}: ", commentId);
        return response;
    }

}

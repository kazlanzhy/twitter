package ua.od.atomspace.twitter.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.od.atomspace.twitter.config.GlobalConfig;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PostDto {

    private String title;

    private String message;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = GlobalConfig.DATE_FORMAT_PATTERN)
    private Date createdAt;

    private Date updatedAt;

    private Long userId;

    private Long postId;
}

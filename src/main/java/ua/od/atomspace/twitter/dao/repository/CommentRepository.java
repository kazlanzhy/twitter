package ua.od.atomspace.twitter.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.od.atomspace.twitter.dao.model.Comment;

public interface CommentRepository extends CrudRepository<Comment, Long> {
}

package ua.od.atomspace.twitter.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.od.atomspace.twitter.dao.model.User;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {

    List<User> findAllBySecondNameEquals(String LastName);

    List<User> findAllByAgeBetween(Short startAge, Short endAge);

    List<User> findTop10ByOrderByCreatedAtDesc();

    Integer countAllByFirstNameStartingWith(String str);

    Boolean existsUserByFollowing(User user);

}

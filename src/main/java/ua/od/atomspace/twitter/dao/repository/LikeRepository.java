package ua.od.atomspace.twitter.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.od.atomspace.twitter.dao.model.Like;
import ua.od.atomspace.twitter.dao.model.ReferenceUserPostKey;

public interface LikeRepository extends CrudRepository<Like, ReferenceUserPostKey> {

}

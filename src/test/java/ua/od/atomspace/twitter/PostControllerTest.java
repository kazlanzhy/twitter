package ua.od.atomspace.twitter;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import ua.od.atomspace.twitter.dto.PostDto;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//todo
public class PostControllerTest extends BaseTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetAllPost() throws Exception {
        mockMvc.perform(get("/api/posts")).andExpect(status().isOk());
    }

    @Test
    public void testGetPost() throws Exception {
        mockMvc.perform(get("/api/posts/1").header("User-Agent", "chrome"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetPostNotFound() throws Exception {
        mockMvc.perform(get("/api/posts/999999").header("User-Agent", "chrome"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetPostWrongUserAgent() throws Exception {
        mockMvc.perform(get("/api/posts/999999").header("User-Agent", "opera"))
                .andExpect(status().is(406));
    }

    @Test
    public void testPostPost() throws Exception {
        String request = mapper.writeValueAsString(PostDto.builder()
                .message("message")
                .title("title")
                .build());
        mockMvc.perform(post("/api/posts")
                .contentType("application/json")
                .content(request))
                .andExpect(status().isCreated())
                .andReturn();
    }

    @Test
    public void testUpdatePost() throws Exception {
        String request = mapper.writeValueAsString(PostDto.builder()
                .title("new title")
                .build());
        mockMvc.perform(put("/api/posts/1")
                .contentType("application/json")
                .content(request))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testDeletePost() throws Exception {
        mockMvc.perform(delete("/api/posts/1")).andExpect(status().isOk());
    }

    @Test
    public void testDeletePostNotFound() throws Exception {
        mockMvc.perform(delete("/api/posts/999999")).andExpect(status().isNotFound());
    }


}

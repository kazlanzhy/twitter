package ua.od.atomspace.twitter;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import ua.od.atomspace.twitter.dto.UserDto;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerTest extends BaseTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetAllUsers() throws Exception {
        mockMvc.perform(get("/api/users")).andExpect(status().isOk());
    }

    @Test
    public void testGetUser() throws Exception {
        mockMvc.perform(get("/api/users/1")).andExpect(status().isOk());
    }

    @Test
    public void testGetUserNotFound() throws Exception {
        mockMvc.perform(get("/api/users/999999")).andExpect(status().isNotFound());
    }

    @Test
    public void testPostUser() throws Exception {
        String request = mapper.writeValueAsString(UserDto.builder()
                .email("test@email.com")
                .age((short)20)
                .firstName("test")
                .secondName("test")
                .password("asdsadasd")
                .username("asdsadsa")
                .build());
        mockMvc.perform(post("/api/users")
                .contentType("application/json")
                .content(request))
                .andExpect(status().isCreated())
                .andReturn();
    }

    @Test
    public void testPostUserInvalidEmail() throws Exception {
        String request = mapper.writeValueAsString(UserDto.builder()
                .email("INVALID_email")
                .age((short)20)
                .firstName("test")
                .secondName("test")
                .password("asdsadasd")
                .username("asdsadsa")
                .build());
        mockMvc.perform(post("/api/users")
                .contentType("application/json")
                .content(request))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void testPostUserInvalidAge() throws Exception {
        String request = mapper.writeValueAsString(UserDto.builder()
                .email("valid@email.com")
                .age((short)5)
                .firstName("test")
                .secondName("test")
                .password("asdsadasd")
                .username("asdsadsa")
                .build());
        mockMvc.perform(post("/api/users")
                .contentType("application/json")
                .content(request))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void testUpdateUserAge() throws Exception {
        String request = mapper.writeValueAsString(UserDto.builder()
                .age((short)15)
                .build());
        mockMvc.perform(put("/api/users/1")
                .contentType("application/json")
                .content(request))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testUpdateUsername() throws Exception {
        String request = mapper.writeValueAsString(UserDto.builder()
                .username("test")
                .build());
        mockMvc.perform(put("/api/users/1")
                .contentType("application/json")
                .content(request))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testDeleteUser() throws Exception {
        mockMvc.perform(delete("/api/users/1")).andExpect(status().isOk());
    }

    @Test
    public void testDeleteUserNotFound() throws Exception {
        mockMvc.perform(delete("/api/users/999999")).andExpect(status().isNotFound());
    }


}

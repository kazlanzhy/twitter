DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS posts;

CREATE TABLE "USERS"
(
    user_id     BIGINT IDENTITY                    NOT NULL,
    age         INT,
    first_name  VARCHAR(50),
    second_name VARCHAR(50),
    username    varchar(30),
    email       varchar(50),
    password    varchar(50),
    created_at  datetime DEFAULT CURRENT_TIMESTAMP,
    updated_at  datetime DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE posts
(
    post_id     BIGINT IDENTITY           NOT NULL,
    title       VARCHAR(70),
    message     VARCHAR(280),
    created_at  datetime DEFAULT CURRENT_TIMESTAMP,
    updated_at  datetime DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO posts (title, message) VALUES ('TEST TITLE', 'TEXTETETETETETETETETETET');
INSERT INTO USERS (age, first_name, second_name, username, email)
            VALUES (19, 'ARTYOM', 'TEST_LAST_NAME', 'TEST_NAME', 'test')

